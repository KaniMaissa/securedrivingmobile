package com.example.pc.securedriving.model;

public interface SmsInterface {

    void onSmsReceived(String message,String sender);
}
