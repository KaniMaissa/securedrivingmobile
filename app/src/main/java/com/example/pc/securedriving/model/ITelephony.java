package com.example.pc.securedriving.model;

public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}