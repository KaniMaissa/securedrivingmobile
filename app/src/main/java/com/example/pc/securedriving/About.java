package com.example.pc.securedriving;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by ArijMelliti on 18/04/2018.
 */

public class About extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_layout);
    }
}