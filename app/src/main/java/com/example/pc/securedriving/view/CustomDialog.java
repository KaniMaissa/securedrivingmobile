package com.example.pc.securedriving.view;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;

import android.view.Gravity;


import com.example.pc.securedriving.R;

public class CustomDialog extends Dialog {
    private static CustomDialog customDialog;
    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    public static CustomDialog createDialog(Context context){

        customDialog = new CustomDialog(context, R.style.Dialog);
        customDialog.setCanceledOnTouchOutside(false);
        customDialog.setContentView(R.layout.dialogsettings);
        customDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        return customDialog ;
    }

}

