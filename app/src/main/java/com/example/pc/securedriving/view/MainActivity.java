package com.example.pc.securedriving.view;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Intent;

import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.pc.securedriving.LocalisationService;
import com.example.pc.securedriving.R;
import com.example.pc.securedriving.model.Utils;
import com.google.firebase.FirebaseApp;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private String FOLDERNAME ="Settings-registred" ;
    private  String RememberHomme="rememberHomme";
    private  String RememberFemme="rememberFemme";
    private  String RememberDay="rememberDay";
    private  String RememberNight="rememberNight";
    private  String RememberMessage="rememberMessage";
    private Boolean selectedhomme,selectedfemme,selectedday,selectednight,selectedmessage;
private SwitchCompat night,day , automessage ;
   RadioButton homme,femme ;
   Button registred,cancel ;
    ImageView call,direction,message,help,menu,arret;
    private int PICK_CONTACT=123;
    private SurfaceHolder surfaceHolder;
    private SurfaceView surfaceView;
    CustomDialog dialog;
    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_bar_main);
        FirebaseApp.initializeApp(getApplicationContext());

        final Intent intent= new Intent(this, LocalisationService.class);

        // hedhi fel mainActivity
        startService(intent);

        Toast.makeText(this, "Started the app", Toast.LENGTH_SHORT).show();



        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
            }
        }
        Toolbar toolbar = findViewById(R.id.toolbar);

        dialog = new CustomDialog(this).createDialog(this);

        Log.i(null , "Video starting");


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);

        View navView =  navigationView.getHeaderView(0);

        navigationView.setNavigationItemSelectedListener(this);
        call=findViewById(R.id.call);
        message=findViewById(R.id.message);
        direction=findViewById(R.id.direction);
        help=findViewById(R.id.help);
        arret=findViewById(R.id.end);

        direction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "http://maps.google.com/maps";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });

        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent =   new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, PICK_CONTACT);



            }
        });
        arret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils. exitApp(MainActivity.this);
            }
        });
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Accueil fragment = new Accueil() ;
        fragmentTransaction.add(R.id.containerfragment,fragment);
        fragmentTransaction.commit();
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        if (null == savedInstanceState) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.surface_camera, CameraFragment.newInstance())
                    .commit();
        }
         }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_READ_PHONE_STATE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permission granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Permission NOT granted: " + PERMISSION_REQUEST_READ_PHONE_STATE, Toast.LENGTH_SHORT).show();
                }

                return;
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        // View v=()v.findViewById(R.layout.nav_header_drawer);
        if (id == R.id.nav_settings) {
            // Handle the camera action

          /*  Intent intent = new Intent(MainActivity.this,CustomDialog.class);
            startActivity(intent);*/
            showDialog(this,"hhh");



        } else if (id == R.id.nav_help) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();


        } else if (id == R.id.nav_about) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();


        } else if (id == R.id.nav_help) {

            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
            finish();

        } else if (id == R.id.nav_close) {
            Utils. exitApp(MainActivity.this);

        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }


public void showDialog (Activity activity, String string)
{AlertDialog.Builder dialog;


    LayoutInflater factory = LayoutInflater.from(MainActivity.this);
    final View alertDialogView = factory.inflate(R.layout.dialogsettings, null);
    dialog = new AlertDialog.Builder(MainActivity.this);
    dialog.setTitle("Confirmation");
    dialog.setView(alertDialogView);

    final AlertDialog ddialog = dialog.create();
    ddialog.show();


   registred= (Button) alertDialogView.findViewById(R.id.register);

    homme=alertDialogView.findViewById(R.id.checkhomme);
    femme=alertDialogView.findViewById(R.id.checkfemme);
day=alertDialogView.findViewById(R.id.switchday);
night=alertDialogView.findViewById(R.id.switchnight);
automessage=alertDialogView.findViewById(R.id.switchmsg);
   cancel = (Button) alertDialogView.findViewById(R.id.annuler);
   cancel.setOnClickListener(
            new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ddialog.dismiss();


                }
            });

    registred.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


               SaveData();
               ddialog.dismiss();

            }
        });

        LoadData();
      Updateviews();
}


    public void SaveData(){
        sharedPreferences = getSharedPreferences(FOLDERNAME,MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.putBoolean(RememberHomme,homme.isChecked());
        editor.putBoolean(RememberFemme,femme.isChecked());
        editor.putBoolean(RememberDay,day.isChecked());
        editor.putBoolean(RememberNight,night.isChecked());
        editor.putBoolean(RememberMessage,automessage.isChecked());
        Toast.makeText(MainActivity.this,"homme "+RememberDay+"Femme "+RememberNight,Toast.LENGTH_LONG).show();
        editor.apply();


    }

    public void LoadData(){
        sharedPreferences=getSharedPreferences(FOLDERNAME,MODE_PRIVATE);
        selectedhomme=sharedPreferences.getBoolean(RememberHomme,false);
        selectedfemme=sharedPreferences.getBoolean(RememberFemme,false);
        selectedday=sharedPreferences.getBoolean(RememberDay,false);
        selectednight=sharedPreferences.getBoolean(RememberNight,false);
        selectedmessage=sharedPreferences.getBoolean(RememberMessage,false);


    }
    public void Updateviews(){
        homme.setChecked(selectedhomme);
        femme.setChecked(selectedfemme);

        day.setChecked(selectedday);
        night.setChecked(selectednight);
        automessage.setChecked(selectedmessage);





    }
}
