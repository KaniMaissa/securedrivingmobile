package com.example.pc.securedriving.view;

public class MessageEvent {
    String messageBody;
    String sender;

    public String getSender() {
        return sender;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public MessageEvent(String messageBody, String sender) {
        this.messageBody = messageBody;
        this.sender = sender;
    }

}
