package com.example.pc.securedriving.view;


import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.securedriving.R;


public class ProgressDialog1 extends Dialog {

    private Context context;
    public ProgressDialog1(Context context) {
        super(context);
        this.context = context;
    }

    public ProgressDialog1(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ProgressDialog1(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
    private static ProgressDialog1 progressDialog;

    public static ProgressDialog1 createDialog(Context context){

        progressDialog = new ProgressDialog1(context, R.style.ProgressDialogStyle);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setContentView(R.layout.progressdialog);
        progressDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        return progressDialog ;
    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (progressDialog == null) {
            return;
        }
        ImageView imageView =  progressDialog.findViewById(R.id.loadingImageView);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.start();
    }

    public ProgressDialog1 setTitle(String title) {
        return progressDialog;
    }

    public ProgressDialog1 setMessage(String strMessage){
        TextView tvMessage = (TextView)progressDialog.findViewById(R.id.id_tv_loadingmsg);

        if (tvMessage != null){
            tvMessage.setText(strMessage);
        }
        return progressDialog;
    }
    public void stopAnim(){
        if (progressDialog == null) {
            return;
        }
        ImageView imageView = (ImageView) progressDialog.findViewById(R.id.loadingImageView);
        AnimationDrawable animationDrawable = (AnimationDrawable) imageView.getBackground();
        animationDrawable.stop();

    }
}