package com.example.pc.securedriving;

import android.Manifest;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Timer;
import java.util.TimerTask;

public class LocalisationService extends Service  {


    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    private Timer mTimer;
    private Handler mHandler = new Handler();

    private LocationManager locManager;
    private LocationListener locListener = new MyLocationListener();

    private boolean gps_enabled = false;
    private boolean network_enabled = false;


    private static final int TIMER_INTERVAL = 30000; // 1min
    private static final int TIMER_DELAY = 0;
    String gps;
    String userId;
    User user;

    //FirebaseDatabase database;
    //DatabaseReference myRef;


    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        if (mTimer != null)
            mTimer = null;

        mTimer = new Timer();

        mTimer.scheduleAtFixedRate(new DisplayToastTimerTask(), TIMER_DELAY, TIMER_INTERVAL);
      //  database = FirebaseDatabase.getInstance();
        //myRef = database.getReference("localisation");

       // FirebaseApp.InitializeApp(Application.Context);



    }



    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        locManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


//        location();
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mTimer.cancel();
        Toast.makeText(getApplicationContext(), "Service destroyed ", Toast.LENGTH_SHORT).show();

    }


    public void location() {
        // exceptions will be thrown if provider is not permitted.
        try {
            gps_enabled = locManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }
        try {
            network_enabled = locManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
        }

        // don't start listeners if no provider is enabled
        if (!gps_enabled & !network_enabled) {
           startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)

);
        }

        if (gps_enabled) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locListener);
        }
        if (network_enabled) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locListener);
        }
    }


    private class DisplayToastTimerTask extends TimerTask {

        @Override
        public void run() {


            mHandler.post(new Runnable() {
                @Override
                public void run() {
location();
                    //Toast.makeText(getApplicationContext(),""+gps,Toast.LENGTH_LONG).show();

                }
            });
        }
    }

    public class MyLocationListener implements LocationListener {

        @Override
        public void onLocationChanged(Location location) {
            if (location != null) {
               /* try {
                    Thread.sleep(3000);                // This needs to stop getting the location data and save the battery power.
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }*/
                locManager.removeUpdates(locListener);

                String lon = ""+location.getLongitude();
                String lat =""+location.getLatitude();
                gps=null;

                gps = (lat + "," + lon);
                User user=new User();
                user.setLoc(gps);
                // Write a message to the database
                    //Toast.makeText(getApplicationContext(),""+gps,Toast.LENGTH_SHORT).show();
                    Log.e("teeeestgps", "Value is: " + gps);

                FirebaseDatabase database = FirebaseDatabase.getInstance();

                FirebaseAuth mAuth = FirebaseAuth.getInstance();
              DatabaseReference myRef = database.getReference("users");
                if(mAuth.getCurrentUser()!=null)
                   userId=mAuth.getCurrentUser().getUid();
                Log.e("loooogauthsucces","" +userId) ;

                myRef.child(userId).child("localisation").child("longitude").setValue(lon);
                myRef.child(userId).child("localisation").child("lattitude").setValue(lat);
               // myRef.setValue(gps);


                // Read from the database
                myRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // This method is called once with the initial value and again
                        // whenever data at this location is updated.
                        //if(dataSnapshot.getValue(String.class)!=null);
                        Log.e("teeeest", "Value is: " );
                    }

                    @Override
                    public void onCancelled(DatabaseError error) {
                        // Failed to read value
                        Log.e("teeeest", "Failed to read value.", error.toException());
                    }
                });

                //gps = (lon + "," + lat);
                //Log.e("gps ", "" + gps);


               }

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }


    }

}
