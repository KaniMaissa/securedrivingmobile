package com.example.pc.securedriving.view;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.speech.tts.TextToSpeech;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.pc.securedriving.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Locale;

import static java.lang.Thread.sleep;

/**
 * A simple {@link Fragment} subclass.
 */

public class Accueil extends Fragment {

    ImageView mic;

    //**************
    SpeechRecognizer recognizer;
    Intent recognizeIntent;
    ArrayList<String> resultStringArrayList;
    RecognitionListener listener;
    String ques = "";
    String msg;
    String c;
    private TextToSpeech tts;
    private ArrayList<String> questions;
    ProgressDialog1 dialog;
    //*************************************************************

    public Accueil() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_accueil, container, false);


//        SmsReceiver smsReceiver = new SmsReceiver();
//
//        smsReceiver.setSmsInterface(this);
//      getActivity().registerReceiver(smsReceiver,new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
        dialog = ProgressDialog1.createDialog(getContext());
        dialog.setMessage("Vous pouvez parler...");


        mic = v.findViewById(R.id.mic);
        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listen();
            }
        });


        // Use OnBoomListener to listen all methods

//*******************************

        //*****Vocale
        tts = new TextToSpeech(getActivity(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status == TextToSpeech.SUCCESS) {
                    int result = tts.setLanguage(Locale.FRANCE);

                    if (result == TextToSpeech.LANG_MISSING_DATA
                            || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                        Log.e("TTS", "Language not supported");
                    } else {
                        mic.setEnabled(true);
                    }
                } else {
                    Log.e("TTS", "Initialization failed");
                }
            }
        });
        listener = new RecognitionListener() {


            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {

                viewChanger(true, "Listening …");
                dialog.onWindowFocusChanged(true);

            }

            @Override
            public void onRmsChanged(float rmsdB) {

            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {
                dialog.cancel();               // stopAnim();
                viewChanger(true, "Chargement …");
            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onError(int error) {
                dialog.stopAnim();
                viewChanger(true, "Aucune entrée vocale");
                try {
                    speak("Aucune entrée vocale vous pouvez parler s'il vous plait");
                    ques = "";
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onResults(Bundle results) {

                resultStringArrayList = results.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION);
                if (resultStringArrayList != null) {
                    try {
                        Toast.makeText(getActivity(), resultStringArrayList.get(0).toString(), Toast.LENGTH_LONG).show();
                        recognition(resultStringArrayList.get(0).toString());
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    viewChanger(true, resultStringArrayList.get(0));
                    // dialog.setMessage(resultStringArrayList.get(0));
                } else {

                    viewChanger(true, "Couldn’t recognize");

                }
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        };
        loadQuestions();


        return v;
    }


    //****************************************************
    private void viewChanger(boolean isEnabled, String s) {
        mic.setEnabled(isEnabled);
//        tv.setText(s);
    }

    public void listen() {
        //initialize the recognizer

        recognizer = SpeechRecognizer.createSpeechRecognizer(getActivity());
        //initialize the intent what will be registered with the recognizer later
        recognizeIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_CALLING_PACKAGE,
                "YOUR FULL PACKAGE NAME OF THAT CLASS");
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true);
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 20);
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_POSSIBLY_COMPLETE_SILENCE_LENGTH_MILLIS, new Long(9000));
      /*  recognizeIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_COMPLETE_SILENCE_LENGTH_MILLIS, new Long (5000));
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_SPEECH_INPUT_MINIMUM_LENGTH_MILLIS, new Long (5000));*/
        recognizeIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH);


        //register the intent with the recognizer
        recognizer.setRecognitionListener(listener);
        recognizer.startListening(recognizeIntent);
        //startAnim();
        dialog.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void appeler(String text) throws InterruptedException {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + get_Number(text, getActivity())));
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        speak("Appel de contact " + text + " en cours");
        sleep(1000);
        startActivity(callIntent);

    }

    private void loadQuestions() {

        questions = new ArrayList<>();
        questions.clear();
//0
        questions.add("Ou voulez vous aller?");
        //1
        questions.add("a qui voulez vous envoyer le message?");
        //2
        questions.add("qui  voulez vous appeler ?");
        //3
        questions.add("quelle est votre méssage  ?");
        //4
        questions.add(" voulez vous envoyer ou modifier ou annuler l'envoi ?");
        //5
        questions.add("quelle est votre recherche ?");
        //6
        // questions.add("écrire l'adresse de votre destinataire et dit envoyer mail  ");
        questions.add("quel est le message du  mail  ? ");
        //7
        questions.add(" voulez vous envoyer le mail ?? ... dite envoyer le mail pour l'envoyer ");
        //8
        questions.add("quel est l'objet du mail  ?");
        //9
        questions.add("a qui voulez vous envoyer le mail?");
        questions.add("quelle est votre recherche d musique ?");


    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void speak(String text) throws InterruptedException {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if ((text.length() == 0) || (text.equals(""))) {
                tts.speak("You haven't typed text", TextToSpeech.QUEUE_FLUSH, null);
                Toast.makeText(getActivity(), "hhhhhhhh", Toast.LENGTH_LONG).show();
            } else {
                tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                dialog.cancel();
            }

        } else {
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
        }
        sleep(1000);
    }

    private void itineraire(String text) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("google.navigation:q=" + text));
        intent.setPackage("com.google.android.apps.maps");
        startActivity(intent);
    }

    private void sendSMS(String phoneNumber, String message) {

        String SENT = "SMS_SENT";

        String DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPI = PendingIntent.getBroadcast(getActivity(), 0,

                new Intent(SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(getActivity(), 0,

                new Intent(DELIVERED), 0);

        //---when the SMS has been sent---

        getActivity().registerReceiver(new BroadcastReceiver() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override

            public void onReceive(Context arg0, Intent arg1) {

                switch (getResultCode()) {

                    case Activity.RESULT_OK:

                        Toast.makeText(getActivity(), "SMS envoyer",

                                Toast.LENGTH_SHORT).show();
                        try {
                            speak("Trés bien Le message a été envoyé avec succès");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        break;

                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:

                        Toast.makeText(getActivity(), " Échec de l'envoi",

                                Toast.LENGTH_SHORT).show();
                        try {
                            speak("désolé Échec de l'envoi");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        break;

                    case SmsManager.RESULT_ERROR_NO_SERVICE:

                        Toast.makeText(getActivity(), "le service est actuellement indisponible",

                                Toast.LENGTH_SHORT).show();
                        try {
                            speak("le service est actuellement indisponible");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        break;

                    case SmsManager.RESULT_ERROR_NULL_PDU:

                        Toast.makeText(getActivity(), "Null PDU",

                                Toast.LENGTH_SHORT).show();

                        break;

                    case SmsManager.RESULT_ERROR_RADIO_OFF:

                        Toast.makeText(getActivity(), "Radio off",

                                Toast.LENGTH_SHORT).show();

                        break;

                }

            }

        }, new IntentFilter(SENT));

        //---when the SMS has been delivered---

        getActivity().registerReceiver(new BroadcastReceiver() {

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override

            public void onReceive(Context arg0, Intent arg1) {

                switch (getResultCode()) {

                    case Activity.RESULT_OK:

                        try {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                speak("Trés bien Le message a été envoyé avec succès");
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        break;

                    case Activity.RESULT_CANCELED:

                        try {
                            speak("désolé Échec de l'envoi");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        break;

                }

            }

        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();

        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);

    }

    private void searchWeb(String text) {
        Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
        String term = text.toString();
        intent.putExtra(SearchManager.QUERY, term);
        startActivity(intent);
    }


    private String get_Number(String name1, Context context) {
        String name = name1.toLowerCase().trim();
        String number = "";
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};

        Cursor people = context.getContentResolver().query(uri, projection, null, null, null);

        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        do {
            String Name = people.getString(indexName);
            String Name1 = Name.toLowerCase().trim();
            //Log.d("name1",Name1);
            String Number = people.getString(indexNumber);
            if ((name.contains(Name1)) || (Name1.contains(name))) {
                return Number.replace("-", "");
            }
            // Do work...
        } while (people.moveToNext());
        if (!number.equalsIgnoreCase("")) {
            return number.replace("-", "");
        } else return number;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void recognition(String text) throws InterruptedException {

        Log.e("Speech", "" + text);
        // String[] speech = text.split(" ");
        Log.d("text", text);

        if (text.toLowerCase().contains("itinéraire")) {
            ques = questions.get(0);
            speak(ques);
            sleep(1000);
            listen();
        } else if (ques.equals("Ou voulez vous aller?")) {
            speak("Le résultat pour " + text);
            itineraire(text);
            ques = "";
        } else if (text.toLowerCase().contains("appeler")) {
            ques = questions.get(2);
            speak(questions.get(2));
            sleep(1000);
            listen();
        } else if (ques.equals(questions.get(2))) {

            if (get_Number(text, getActivity()) == "") {
                speak("je ne pas trouver le contact ,,, vous pouvez répéter !");
                sleep(3000);

                ques = questions.get(2);
                speak(questions.get(2));
                listen();

            } else {
                appeler(text);
                ques = "";
            }
        } else if ((text.contains("envoyer un message")) || (text.contains("envoyer message")) || (text.contains("envoyer le message"))) {
            ques = questions.get(1);
            speak(questions.get(1));
            sleep(1300);
            listen();
        } else if (ques.equals(questions.get(1))) {
            c = get_Number(text, getActivity());
            if (c == "") {
                speak("désoler je ne pas trouver le contact  vous pouvez répéter !");
                sleep(3000);
                ques = questions.get(1);
                speak(questions.get(1));
                sleep(1000);
                listen();
            } else {
                speak(questions.get(3));
                Toast.makeText(getActivity(), c, Toast.LENGTH_LONG).show();
                ques = questions.get(3);
                sleep(1000);
                listen();
            }
        } else if (ques.equals(questions.get(3))) {

            msg = text;
            Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
            speak("" + msg.toString());
            sleep(2000);

            speak(questions.get(4));
            sleep(1111);
            ques = "";
            listen();
        } else if (text.equals("envoyer")) {
            sendSMS(c, msg);
        } else if (text.equals("modifier")) {
            ques = questions.get(3);
            speak(questions.get(3));
            sleep(999);
            listen();

        } else if ((text.contains("annuler l'envoi")) || (text.contains("annuler message")) || (text.contains("annuler"))) {
            speak("ok , l'envoi de message annuler");
        } else if ((text.toLowerCase().contains("recherche google")) || (text.toLowerCase().contains("google"))) {
            speak(questions.get(5));
            sleep(999);
            ques = questions.get(5);
            listen();
        } else if (ques.equals(questions.get(5))) {
            ques = "";
            try {
                searchWeb(text);
            } catch (Exception e) {

            }
        } else {
            speak("je ne peut pas répondre la demande" + text.toString());
            sleep(3000);
            speak("vous pouver reparler maitenant");
            sleep(1000);
            listen();
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEventReceived(MessageEvent messageEvent) {
        Log.d("aaaaaaaaaaaaaa", "onReceive: ");

        if (messageEvent != null && messageEvent.getMessageBody() != null) {
            try {
                speak(messageEvent.getMessageBody() + "De la part de  " + messageEvent.getSender());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}


