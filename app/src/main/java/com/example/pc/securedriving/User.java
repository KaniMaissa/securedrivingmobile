package com.example.pc.securedriving;


 import com.google.firebase.database.IgnoreExtraProperties;



@IgnoreExtraProperties
public class User {

    public String name;
    public String prenom;
    public String loc;
    public String imma;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public User() {
    }



    public User(String name, String prenom, String loc, String imma) {
        this.name = name;
        this.prenom = prenom;
        this.loc = loc;
        this.imma = imma;

    }

    public String getImmatricule() {
        return imma;
    }

    public void setImmatricule(String imma) {
        this.imma = imma;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
