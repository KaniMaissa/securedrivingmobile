package com.example.pc.securedriving;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;


import android.view.ViewGroup.LayoutParams;
import android.widget.TextView;

public class CustomDialog extends Dialog {
    private static CustomDialog customDialog;
    public CustomDialog(@NonNull Context context) {
        super(context);
    }

    public CustomDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }
    public static CustomDialog createDialog(Context context){

        customDialog = new CustomDialog(context, R.style.Dialog);
        customDialog.setCanceledOnTouchOutside(false);
        customDialog.setContentView(R.layout.dialogsettings);
        customDialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        return customDialog ;
    }

}

