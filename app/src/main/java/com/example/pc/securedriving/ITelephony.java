package com.example.pc.securedriving;

public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}