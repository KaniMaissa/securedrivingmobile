package com.example.pc.securedriving;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pc.securedriving.view.MainActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LogActivity extends AppCompatActivity {

    private static final String TAG = LogActivity.class.getSimpleName();

    EditText txtmail,mobile, txtpwd, txtnom,txtprenom,txtloc;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private String no,pwd, userId;
    Button button,btn;
    private FirebaseAuth mAuth;
    FirebaseDatabase database ;
    DatabaseReference myRef ;
    TextView txt;
    User user;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get Firebase auth instance
        FirebaseApp.initializeApp(getApplicationContext());

       mAuth = FirebaseAuth.getInstance();

       if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            userId=mAuth.getCurrentUser().getUid();
            Log.e("loooogauthsucces","" +userId) ;
           startActivity(new Intent(LogActivity.this, MainActivity.class));
           finish();


        }

        setContentView(R.layout.activity_log);

        // Displaying toolbar icon

        //mobile = (EditText) findViewById(R.id.mobile);

        button = (Button) findViewById(R.id.button);
        txtnom = findViewById(R.id.txtname);
        txtprenom = findViewById(R.id.txtprenom);
        txtloc = findViewById(R.id.txtloc);

        txtmail = findViewById(R.id.txtmail);
        txtpwd = findViewById(R.id.edpwd);

         user = new User();


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //no = mobile.getText().toString();

                user.setName(txtnom.getText().toString());
                user.setPrenom(txtprenom.getText().toString());
                user.setImmatricule(txtloc.getText().toString());
               no=txtmail.getText().toString();
                pwd=txtpwd.getText().toString();
                if (pwd.isEmpty() || pwd.length() < 6) {
                    txtpwd.setError("password must be more than 5 letter");
                    txtpwd.requestFocus();
                }else{
                mAuth.createUserWithEmailAndPassword(no, pwd)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {


                                    if(mAuth.getCurrentUser()!=null)
                                        userId=mAuth.getCurrentUser().getUid();
                                    Log.e("loooogauthsucces","" +userId) ;

                                    // myRef.child(userId).setValue(user);
                                    //verification successful we will start the profile activity
                                    updateUser(user.getName(),user.getPrenom(), user.getLoc(), userId);



                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Login failed! Please try again later", Toast.LENGTH_LONG).show();
                                    //progressBar.setVisibility(View.GONE);
                                    Log.e("error is :", task.getException().getMessage());
                                }
                            }
                        });}
                //verifying the code entered manually
                // verifyVerificationCode(code);


                Toast.makeText(LogActivity.this, no, Toast.LENGTH_LONG).show();
            }
        });







       /* txtmail=findViewById(R.id.textView2);
         txtpwd=findViewById(R.id.textView3);
        btn=findViewById(R.id.button2);



        mFirebaseInstance = FirebaseDatabase.getInstance();

        // get reference to 'users' node
        mFirebaseDatabase = mFirebaseInstance.getReference("users");

        // store app title to 'app_title' node
        mFirebaseInstance.getReference("app_title").setValue("Realtime Database");

        // app_title change listener
        mFirebaseInstance.getReference("app_title").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "App title updated");

                String appTitle = dataSnapshot.getValue(String.class);

                // update toolbar title
                getSupportActionBar().setTitle(appTitle);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read app title value.", error.toException());
            }
        });

        // Save / update the user
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = txtpwd.getText().toString();
                String email = txtmail.getText().toString();

                // Check for already existed userId
                if (TextUtils.isEmpty(userId)) {
                    createUser(name, email);
                } else {
                    updateUser(name, email);
                }
            }
        });

        toggleButton();
    }

    // Changing button text
    private void toggleButton() {
        if (TextUtils.isEmpty(userId)) {
            btn.setText("Save");
        } else {
            btn.setText("Update");
        }
    }


      ///creating new user node under 'users'

    private void createUser(String name, String email) {
        // TODO
        // In real apps this userId should be fetched
        // by implementing firebase auth
        if (TextUtils.isEmpty(userId)) {
            userId = mFirebaseDatabase.push().getKey();
        }

        User user = new User(name, email);

        mFirebaseDatabase.child(userId).setValue(user);

        addUserChangeListener();
    }

    //User data change listener

    private void addUserChangeListener() {
        // User data change listener
        mFirebaseDatabase.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);

                // Check for null
                if (user == null) {
                    Log.e(TAG, "User data is null!");
                    return;
                }

                Log.e(TAG, "User data is changed!" + user.name + ", " + user.email);

                // Display newly updated name and email
               // txtDetails.setText(user.name + ", " + user.email);

                // clear edit text
                txtmail.setText("");
                txtpwd.setText("");

                toggleButton();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.e(TAG, "Failed to read user", error.toException());
            }
        });
    }

    private void updateUser(String name, String email) {
        // updating the user via child nodes
        if (!TextUtils.isEmpty(name))
            mFirebaseDatabase.child(userId).child("name").setValue(name);

        if (!TextUtils.isEmpty(email))
            mFirebaseDatabase.child(userId).child("email").setValue(email);
    }
}

*/


    }
    private void updateUser(String name, String prenom, String loc, String userid) {
        // updating the user via child nodes
       // User user=new User();
        Log.e("testuser",""+user.getName());
        database = FirebaseDatabase.getInstance();
          myRef = database.getReference("users");

            myRef.child(userid).child("name").setValue(user.getName());


            myRef.child(userid).child("prenom").setValue(user.getPrenom());


            myRef.child(userid).child("immatricule").setValue(user.getImmatricule());




        startActivity(new Intent(LogActivity.this, MainActivity.class));
        finish();
    }
        private void validNo (String pwd){
            if (pwd.isEmpty() || pwd.length() < 6) {
                txtpwd.setError("password must be more than 5 letter");
                txtpwd.requestFocus();
                return;
            }
        }
    }

