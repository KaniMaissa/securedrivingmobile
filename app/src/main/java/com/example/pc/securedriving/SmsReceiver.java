package com.example.pc.securedriving;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.provider.ContactsContract;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;


public class SmsReceiver extends BroadcastReceiver {
    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    String messageBody;

    StringBuilder sb;

    public SmsReceiver() {
        super();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        //if (intent.getAction().equals(SMS_RECEIVED)) {
        Log.d("aaaaaaaaaaaaaa", "onReceive: ");
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                // get sms objects
                Object[] pdus = (Object[]) bundle.get("pdus");
                if (pdus.length == 0) {
                    return;
                }
                // large message might be broken into many
                SmsMessage[] messages = new SmsMessage[pdus.length];
                sb = new StringBuilder();
                for (int i = 0; i < pdus.length; i++) {
                    messages[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    sb.append(messages[i].getMessageBody());
                }

                String sender = messages[0].getOriginatingAddress();

                messageBody = sb.toString();
                String ContactName =" " ;

                Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(sender));
                Cursor c = context.getContentResolver().query(lookupUri, new String[]{ContactsContract.Data.DISPLAY_NAME},null,null,null);
                try {
                    c.moveToFirst();
                    String  displayName = c.getString(0);
                  ContactName = displayName;
                    Toast.makeText(context, ContactName, Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    // TODO: handle exception
                }finally{
                    c.close();
                }
                EventBus.getDefault().post(new MessageEvent(messageBody,ContactName));

            }


        }




}


