package com.example.pc.securedriving;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.example.pc.securedriving.view.MainActivity;

import java.util.concurrent.TimeUnit;

public class VerifyMobile extends AppCompatActivity {


    EditText otp;
    Button login;
    String no;
     FirebaseAuth mAuth;
    private String mVerificationId;
    FirebaseDatabase database;
    DatabaseReference myRef;
    private String userId;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_mobile);


        otp = (EditText) findViewById(R.id.otp);
        mAuth = FirebaseAuth.getInstance();

        no = getIntent().getStringExtra("mobile");
        user = new User();



        database = FirebaseDatabase.getInstance();

        myRef = database.getReference("users");
        //if(mAuth.getCurrentUser()!=null)
        //userId=mAuth.getCurrentUser().getUid();

        login = (Button) findViewById(R.id.login);





        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               /* String code = otp.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    otp.setError("Enter valid code");
                    otp.requestFocus();
                    return;*/


                mAuth.createUserWithEmailAndPassword(no, "123456")
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {


                                    if(mAuth.getCurrentUser()!=null)
                                        userId=mAuth.getCurrentUser().getUid();
                                    Log.e("loooogauthsucces","" +userId) ;

                                   // myRef.child(userId).setValue(user);
                                    //verification successful we will start the profile activity
                                    updateUser(user.getName(),user.getPrenom(), user.getLoc(), userId);



                                }
                                else {
                                    Toast.makeText(getApplicationContext(), "Login failed! Please try again later", Toast.LENGTH_LONG).show();
                                    //progressBar.setVisibility(View.GONE);
                                    Log.e("error is :", task.getException().getMessage());
                                }
                            }
                        });
                //verifying the code entered manually
               // verifyVerificationCode(code);

            }
        });
        //User user = new User(name, prenom, loc, "");


    }

    // get reference to 'users' node

        // store app title to 'app_title' node
        // database.getReference("app_title").setValue("Realtime Database");

        // app_title change listener

        // Save / update the user







    private void updateUser(String name, String prenom, String loc, String userid) {
        // updating the user via child nodes




            myRef.child(userid).child("name").setValue(user.getName());


            myRef.child(userid).child("prenom").setValue(user.getPrenom());

            myRef.child(userid).child("immatricule").setValue(user.getImmatricule());

        Intent intent = new Intent(VerifyMobile.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }



}